stations = int(input()) #вводим количество станций

st = [] #будет храниться информация о кол-ве пассажиров на станции

for i in range(stations):
    st.append(0) #заполняем список нулями

n = 3

for i in range(n):
    passenger = input()
    passenger = passenger.split()
    n1 = int(passenger[2])
    n2 = int(passenger[3])
    for i in range(n1, n2):
        st[i] = st[i] + 1 #вводим пассажиров и проверяем на каких станциях они были, заполняем список

max = 0

for i in range(stations):
    if st[i] > max:
        max = st[i] #находим максимальное кол-во пассажиров

for i in range(1, stations):
    if st[i] == max:
        print(i, "-", i + 1) #проверяем на каких станциях максимальное количество пассажиров и выводим